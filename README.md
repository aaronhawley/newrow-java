New Row Java API prototype
==========================

Interfacing with New Row, Class Connect and Blackboard Collaborate.

## Getting started

Clone this repo, and then change branches:

    $ git checkout develop

Start writing tests and run them:

    $ mvn test
